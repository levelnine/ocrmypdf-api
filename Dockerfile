FROM php:8.1-fpm-bullseye

ARG OCRMYPDF_VERSION="v14.0.3"

ENV LANG=C.UTF-8
ENV TZ=UTC
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections

# Install PHP dependencies
RUN apt-get update -y \
    && apt-get install -y build-essential openssl zip unzip git libonig-dev zlib1g-dev libpng-dev libzip-dev libfreetype6-dev libjpeg62-turbo-dev libxml2-dev \
    && apt-get install -y libpangocairo-1.0-0 libx11-xcb1 libxcomposite1 libxcursor1 libxdamage1 libxi6 libxtst6 libnss3 libcups2 libxss1 libxrandr2 libgconf-2-4 libasound2 libatk1.0-0 libgtk-3-0 \
    && apt-get install -y libgbm-dev
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN docker-php-ext-install bcmath zip pdo pdo_mysql opcache soap exif intl
RUN docker-php-ext-configure gd --with-freetype --with-jpeg
RUN docker-php-ext-install gd

COPY docker/php-fpm/logging.conf /usr/local/etc/php-fpm.d/logging.conf

RUN pecl install -o -f redis \
    &&  docker-php-ext-enable redis

RUN apt-get update && apt-get install -y --no-install-recommends \
  python3 \
  libqpdf-dev \
  zlib1g \
  liblept5

# Note we need leptonica here to build jbig2
RUN apt-get update && apt-get install -y --no-install-recommends \
  build-essential autoconf automake libtool \
  libleptonica-dev \
  zlib1g-dev \
  python3-dev \
  python3-distutils \
  libffi-dev \
  ca-certificates \
  curl \
  git

# Get the latest pip (Ubuntu version doesn't support manylinux2010)
RUN \
  curl https://bootstrap.pypa.io/get-pip.py | python3

# Compile and install jbig2
# Needs libleptonica-dev, zlib1g-dev
RUN \
  mkdir jbig2 \
  && curl -L https://github.com/agl/jbig2enc/archive/ea6a40a.tar.gz | \
  tar xz -C jbig2 --strip-components=1 \
  && cd jbig2 \
  && ./autogen.sh && ./configure && make && make install \
  && cd .. \
  && rm -rf jbig2

#COPY ./OCRmyPDF/* /ocrmypdf

WORKDIR /ocrmypdf

#RUN pip3 install --no-cache-dir .[test,webservice,watcher]

RUN pip3 install --no-cache-dir git+https://github.com/ocrmypdf/OCRmyPDF@$OCRMYPDF_VERSION

# For Tesseract 5
RUN apt-get update && apt-get install -y --no-install-recommends \
  software-properties-common gpg-agent
#RUN add-apt-repository -y ppa:alex-p/tesseract-ocr-devel

RUN apt-get update && apt-get install -y --no-install-recommends \
  ghostscript \
  jbig2dec \
  img2pdf \
  libsm6 libxext6 libxrender-dev \
  pngquant \
  tesseract-ocr \
  tesseract-ocr-eng \
  tesseract-ocr-spa \
  unpaper \
  && rm -rf /var/lib/apt/lists/*

# OCRMyPDF entrypoint here

# Now build application

WORKDIR /app

# Install php dependencies
COPY composer.json composer.lock /app/
RUN composer install --no-autoloader

RUN echo 'memory_limit = ${PHP_MEMORY_LIMIT}' >> /usr/local/etc/php/conf.d/docker-php-memory-limit.ini
RUN echo 'upload_max_filesize = ${PHP_UPLOAD_MAX_FILESIZE}' >> /usr/local/etc/php/conf.d/docker-php-max-upload-size.ini
RUN echo 'post_max_size = ${PHP_MAX_POST_SIZE}' >> /usr/local/etc/php/conf.d/docker-php-max-post-size.ini
RUN echo 'max_execution_time = ${PHP_MAX_EXECUTION_TIME}' >> /usr/local/etc/php/conf.d/docker-php-max-execution-time.ini

COPY . /app

RUN composer dump-autoload -o
