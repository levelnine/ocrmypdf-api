<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('ocr_jobs', function (Blueprint $table) {
            $table->id();
            $table->uuid()->unique()->index();
            $table->foreignIdFor(\App\Models\User::class);
            $table->string('original_path');
            $table->string('completed_path')->nullable();
            $table->longText('text')->nullable();
            $table->string('queue_job_id')->nullable();
            $table->uuid('queue_job_uuid')->nullable();
            $table->boolean('is_running')->default(false);
            $table->timestamp('started_at', 6)->nullable();
            $table->timestamp('completed_at', 6)->nullable();
            $table->timestamp('failed_at', 6)->nullable();
            $table->longText('exception')->nullable();
            $table->timestamps(6);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('job_statuses');
    }
};
