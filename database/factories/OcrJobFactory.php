<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\OcrJob>
 */
class OcrJobFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'uuid' => fake()->uuid(),
            'queue_job_id' => Str::random(),
            'queue_job_uuid' => fake()->uuid(),
            'user_id' => User::factory(),
            'original_path' => fake()->filePath(),
            'completed_path' => null,
            'text' => null,
            'is_running' => false,
            'started_at' => null,
            'completed_at' => null,
            'failed_at' => null,
            'exception' => null,
        ];
    }
}
