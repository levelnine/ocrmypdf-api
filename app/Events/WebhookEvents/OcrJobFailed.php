<?php

namespace App\Events\WebhookEvents;

use App\Exceptions\OCRmyPDF\OcrMyPdfException;
use App\Models\OcrJob;

class OcrJobFailed extends AbstractWebhookEvent
{
    public function __construct(
        public OcrJob $ocrJob,
        public OcrMyPdfException $exception)
    {
        parent::__construct($this->ocrJob);
    }

    public string $type = 'ocr.job.failed';
}
