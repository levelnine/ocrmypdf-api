<?php

namespace App\Events\WebhookEvents;

class OcrJobCompleted extends AbstractWebhookEvent
{
    public string $type = 'ocr.job.completed';
}
