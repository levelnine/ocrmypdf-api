<?php

namespace App\Events\WebhookEvents;

class OcrJobCreated extends AbstractWebhookEvent
{
    public string $type = 'ocr.job.created';
}
