<?php

namespace App\Events\WebhookEvents;

class OcrJobStarted extends AbstractWebhookEvent
{
    public string $type = 'ocr.job.started';
}
