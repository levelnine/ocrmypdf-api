<?php

namespace App\Events\WebhookEvents;

use App\Models\OcrJob;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

abstract class AbstractWebhookEvent
{
    use Dispatchable, SerializesModels;

    public string $type;

    protected string $timestamp;

    /**
     * Create a new event instance.
     */
    public function __construct(
        public OcrJob $ocrJob
    ) {
        $this->timestamp = microtime();
    }

    public function getTimestamp(): int
    {
        return $this->timestamp;
    }
}
