<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use Throwable;

/**
 * @property int                                                 $id
 * @property string|null                                         $uuid
 * @property int                                                 $user_id
 * @property string|null                                         $queue_job_id
 * @property string|null                                         $queue_job_uuid
 * @property bool                                                $is_running
 * @property string|null                                         $original_path
 * @property string|null                                         $completed_path
 * @property string|null                                         $ocr_text
 * @property \Carbon\Carbon|null                                 $started_at
 * @property \Carbon\Carbon|null                                 $completed_at
 * @property \Carbon\Carbon|null                                 $failed_at
 * @property string|null                                         $exception
 * @property \Carbon\Carbon                                      $created_at
 * @property \Carbon\Carbon                                      $updated_at
 * @property \App\Models\User|\Illuminate\Database\Query\Builder $user
 */
class OcrJob extends Model
{
    use HasFactory;

    protected static function boot()
    {
        parent::boot();

        static::creating(function (OcrJob $ocrJob) {
            if (blank($ocrJob->uuid)) {
                $ocrJob->uuid = Str::uuid()->toString();
            }
        });
    }

    protected $fillable = [
        'uuid',
        'user_id',
        'queue_job_id',
        'queue_job_uuid',
        'original_path',
        'completed_path',
        'text',
        'is_running',
        'started_at',
        'completed_at',
        'failed_at',
        'exception',
    ];

    protected $casts = [
        'is_running' => 'boolean',
        'started_at' => 'datetime',
        'completed_at' => 'datetime',
        'failed_at' => 'datetime',
    ];

    public function getRouteKeyName(): string
    {
        return 'uuid';
    }

    /**
     * Fetch the user (project) this file belongs to
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function setQueueJobInformation(string $id, string $uuid): bool
    {
        return $this->update([
            'queue_job_id' => $id,
            'queue_job_uuid' => $uuid,
        ]);
    }

    public function setAsStarted(): bool
    {
        return $this->update([
            'started_at' => now(),
            'is_running' => true,
        ]);
    }

    public function setAsCompleted(string $completedPath, ?string $text): bool
    {
        return $this->update([
            'completed_at' => now(),
            'is_running' => false,
            'completed_path' => $completedPath,
            'text' => $text,
        ]);
    }

    public function setAsFailed(Throwable $exception): bool
    {
        return $this->update([
            'failed_at' => now(),
            'exception' => (string) $exception,
            'is_running' => false,
        ]);
    }

    public function downloadUrl(): string
    {
        return URL::signedRoute('ocr.jobs.file.download', [
            'ocrJob' => $this,
        ]);
    }

    public function isCompleted(): bool
    {
        return filled($this->started_at)
            && filled($this->completed_at)
            && $this->is_running === false
            && blank($this->exception)
            && blank($this->failed_at);
    }
}
