<?php

namespace App\Listeners;

use App\Events\WebhookEvents\AbstractWebhookEvent;
use App\Http\Resources\OcrJobResource;
use Spatie\WebhookServer\WebhookCall;

class SendWebhookEvent
{
    /**
     * Handle the event.
     */
    public function handle(AbstractWebhookEvent $event): void
    {
        WebhookCall::create()
            ->url($event->ocrJob->user->webhook_url)
            ->useSecret($event->ocrJob->user->webhook_secret)
            ->payload([
                'object' => 'event',
                'created' => (string) microtime(true),
                'type' => $event->type,
                'data' => json_decode((new OcrJobResource($event->ocrJob))->toJson(), true),
            ])
            ->meta([
                'ocr_job_uuid' => $event->ocrJob->uuid,
            ])
            ->onQueue('webhooks')
            ->dispatch();
    }
}
