<?php

namespace App\Exceptions\OCRmyPDF;

use Exception;

class AlreadyDoneOcrException extends Exception
{
    /** The error message */
    protected $message = '';

    /** The error code */
    protected $code = 6;
}
