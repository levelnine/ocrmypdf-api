<?php

namespace App\Exceptions\OCRmyPDF;

use Exception;

class BadArgumentsException extends Exception
{
    /** The error message */
    protected $message = '';

    /** The error code */
    protected $code = 1;
}
