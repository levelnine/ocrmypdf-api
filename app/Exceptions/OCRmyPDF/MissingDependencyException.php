<?php

namespace App\Exceptions\OCRmyPDF;

use Exception;

class MissingDependencyException extends Exception
{
    /** The error message */
    protected $message = 'A third-party dependency is missing.';

    /** The error code */
    protected $code = 3;
}
