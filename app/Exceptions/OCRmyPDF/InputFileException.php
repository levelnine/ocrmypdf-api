<?php

namespace App\Exceptions\OCRmyPDF;

use Exception;

class InputFileException extends Exception
{
    /** The error message */
    protected $message = 'The input file format is not supported.';

    /** The error code */
    protected $code = 2;
}
