<?php

namespace App\Exceptions\OCRmyPDF;

use Exception;

class EncryptedPdfException extends Exception
{
    /** The error message */
    protected $message = '';

    /** The error code */
    protected $code = 8;
}
