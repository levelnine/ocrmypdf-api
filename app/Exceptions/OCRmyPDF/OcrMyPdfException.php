<?php

namespace App\Exceptions\OCRmyPDF;

use App\Models\OcrJob;
use Exception;
use Throwable;

class OcrMyPdfException extends Exception
{
    /** @see https://github.com/ocrmypdf/OCRmyPDF/blob/7bd0e43243a05e56a92d6b00fcaa3c826fb3cccd/src/ocrmypdf/exceptions.py#L12 */
    public const EXIT_CODE_OK = 0;

    public const EXIT_CODE_BAD_ARGS = 1;

    public const EXIT_CODE_INPUT_FILE = 2;

    public const EXIT_CODE_MISSING_DEPENDENCY = 3;

    public const EXIT_CODE_INVALID_OUTPUT_PDF = 4;

    public const EXIT_CODE_FILE_ACCESS_ERROR = 5;

    public const EXIT_CODE_ALREADY_DONE_OCR = 6;

    public const EXIT_CODE_CHILD_PROCESS_ERROR = 7;

    public const EXIT_CODE_ENCRYPTED_PDF = 8;

    public const EXIT_CODE_INVALID_CONFIG = 9;

    public const EXIT_CODE_PDFA_CONVERSION_FAILED = 10;

    public const EXIT_CODE_OTHER_ERROR = 15;

    public const EXCEPTIONS_MAP = [
        self::EXIT_CODE_BAD_ARGS => BadArgumentsException::class,
        self::EXIT_CODE_INPUT_FILE => InputFileException::class,
        self::EXIT_CODE_MISSING_DEPENDENCY => MissingDependencyException::class,
        self::EXIT_CODE_INVALID_OUTPUT_PDF => InvalidOutputPdfException::class,
        self::EXIT_CODE_FILE_ACCESS_ERROR => FileAccessException::class,
        self::EXIT_CODE_ALREADY_DONE_OCR => AlreadyDoneOcrException::class,
        self::EXIT_CODE_CHILD_PROCESS_ERROR => ChildProcessException::class,
        self::EXIT_CODE_ENCRYPTED_PDF => EncryptedPdfException::class,
        self::EXIT_CODE_INVALID_CONFIG => InvalidConfigException::class,
        self::EXIT_CODE_PDFA_CONVERSION_FAILED => PdfConversionFailedException::class,
        self::EXIT_CODE_OTHER_ERROR => UnknownException::class,
    ];

    public OcrJob $ocrJob;

    public function __construct(
        OcrJob $ocrJob,
        string $message = '',
        int $code = 0,
        ?Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);

        $this->ocrJob = $ocrJob;
    }
}
