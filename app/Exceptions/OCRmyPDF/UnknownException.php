<?php

namespace App\Exceptions\OCRmyPDF;

class UnknownException extends OcrMyPdfException
{
    /** The error message */
    protected $message = 'An unknown error has occurred.';

    /** The error code */
    protected $code = 15;
}
