<?php

namespace App\Exceptions\OCRmyPDF;

use Exception;

class InvalidOutputPdfException extends Exception
{
    /** The error message */
    protected $message = 'Cannot access the intended output file path.';

    /** The error code */
    protected $code = 4;
}
