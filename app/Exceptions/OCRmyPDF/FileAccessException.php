<?php

namespace App\Exceptions\OCRmyPDF;

use Exception;

class FileAccessException extends Exception
{
    /** The error message */
    protected $message = 'Cannot access the intended file path.';

    /** The error code */
    protected $code = 5;
}
