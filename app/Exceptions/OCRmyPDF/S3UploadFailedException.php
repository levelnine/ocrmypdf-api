<?php

namespace App\Exceptions\OCRmyPDF;

class S3UploadFailedException extends OcrMyPdfException
{
    protected $message = 'There was an error while uploading the processed file to S3.';

    protected $code = 51;
}
