<?php

namespace App\Exceptions\OCRmyPDF;

class S3StreamToLocalFailedException extends OcrMyPdfException
{
    protected $message = 'Failed to stream original file from S3 to Local';

    protected $code = 50;
}
