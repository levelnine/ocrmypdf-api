<?php

namespace App\Exceptions\OCRmyPDF;

use Exception;

class InvalidConfigException extends Exception
{
    /** The error message */
    protected $message = 'An error occurred while parsing a Tesseract configuration file.';

    /** The error code */
    protected $code = 9;
}
