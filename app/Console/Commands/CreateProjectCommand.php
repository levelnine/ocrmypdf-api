<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Console\ConfirmableTrait;
use Illuminate\Support\Str;

class CreateProjectCommand extends Command
{
    use ConfirmableTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:create-project';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new Project (aka User).';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        // Confirm if the application environment is in production...
        $this->confirmToProceed();

        $name = $this->ask('What is the project name?');
        $environment = $this->choice('What environment will this token be used for?', [
            'local', 'staging', 'feature', 'production', 'other',
        ]);

        $environment = ($environment === 'other')
            ? $this->ask('What is the name of this environment?')
            : $environment;

        $webhookUrl = $this->ask("What is the project's webhook URL?");

        // TODO this does not handle if there is already a project/token with
        //      the same information already created
        /** @var \App\Models\User $user */
        $user = User::create([
            'name' => $name,
            'environment' => $environment,
            'webhook_url' => $webhookUrl,
            'webhook_secret' => Str::random(32),
        ]);

        $apiToken = $user->createToken('api-token');

        $this->table(
            ['id', 'name', 'environment', 'webhook_url', 'api_token', 'webhook_secret'],
            [
                [
                    $user->uuid,
                    $user->name,
                    $user->environment,
                    $user->webhook_url,
                    $apiToken->plainTextToken,
                    $user->webhook_secret,
                ],
            ]
        );
    }
}
