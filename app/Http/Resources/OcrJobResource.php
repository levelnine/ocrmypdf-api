<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class OcrJobResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->uuid,
            'completed' => $this->isCompleted(),
            'text' => $this->text ?? null,
            'original_path' => filled($this->original_path) ? Storage::url($this->original_path) : null,
            'completed_path' => filled($this->completed_path) ? Storage::url($this->completed_path) : null,
            'download_url' => ($this->isCompleted()) ? $this->downloadUrl() : null,
            'is_running' => $this->is_running,
            'started_at' => $this->started_at,
            'completed_at' => $this->completed_at,
            'failed_at' => $this->failed_at,
            'exception' => $this->short_exception,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
