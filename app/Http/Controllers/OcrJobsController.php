<?php

namespace App\Http\Controllers;

use App\Events\WebhookEvents\OcrJobCreated;
use App\Http\Resources\OcrJobResource;
use App\Jobs\OcrMyPdfJob;
use App\Models\File;
use App\Models\OcrJob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\StreamedResponse;

class OcrJobsController extends Controller
{
    /**
     * Store a newly created resource in storage for processing.
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(Request $request): OcrJobResource
    {
        $this->authorize('create', OcrJob::class);

        $request->validate([
            'file' => [
                'required',
                'file',
                'mimetypes:application/pdf',
                'max:2100000', // Value is in kilobytes, total: ~2GB
            ],
        ]);

        $ocrJob = new OcrJob();

        // User ID is technically just a Project ID.
        // Within this application the concept of a Project
        // is represented via the User's model to be able to
        // utilize laravel/sanctum. Because LAMBOs.
        $ocrJob->user_id = $request->user()->id;

        $ocrJob->uuid = Str::uuid()->toString();

        // Create the file's directory for usage...
        Storage::disk('s3')
            ->makeDirectory($ocrJob->uuid);

        // Upload the file to storage...
        $ocrJob->original_path = $request->file
            ->storeAs($ocrJob->uuid, 'original.pdf', 's3');

        $ocrJob->save();

        event(new OcrJobCreated($ocrJob));

        dispatch(new OcrMyPdfJob($ocrJob))->onQueue('ocr');

        return new OcrJobResource($ocrJob);
    }

    /**
     * View the File Status Information
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function view(OcrJob $ocrJob): OcrJobResource
    {
        $this->authorize('view', $ocrJob);

        return new OcrJobResource($ocrJob);
    }

    /**
     * Download the given file from storage.
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function download(OcrJob $ocrJob): StreamedResponse
    {
        $this->authorize('view', $ocrJob);

        // TODO is this really the response that should be used?
        abort_unless($ocrJob->completed_path, 500);

        return Storage::download(
            $ocrJob->completed_path,
            "{$ocrJob->uuid}_ocr.pdf"
        );
    }
}
