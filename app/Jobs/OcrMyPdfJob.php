<?php

namespace App\Jobs;

use App\Events\WebhookEvents\OcrJobCompleted;
use App\Events\WebhookEvents\OcrJobFailed;
use App\Events\WebhookEvents\OcrJobStarted;
use App\Exceptions\OCRmyPDF\OcrMyPdfException;
use App\Exceptions\OCRmyPDF\S3StreamToLocalFailedException;
use App\Exceptions\OCRmyPDF\S3UploadFailedException;
use App\Exceptions\OCRmyPDF\UnknownException;
use App\Models\File;
use App\Models\OcrJob;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Process;
use Illuminate\Support\Facades\Storage;
use Throwable;

class OcrMyPdfJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public OcrJob $ocrJob;

    protected array $shouldNotifyExceptions = [];

    protected Filesystem $ocrJobsDisk;

    protected Filesystem $s3disk;

    /**
     * Create a new job instance.
     */
    public function __construct(OcrJob $ocrJob)
    {
        $this->ocrJob = $ocrJob;
        $this->shouldNotifyExceptions = config('ocr.exceptions_via_webhooks', []);
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $this->ocrJob->setQueueJobInformation($this->job->getJobId(), $this->job->uuid());

        $this->ocrJob->setAsStarted();

        event(new OcrJobStarted($this->ocrJob));

        $this->ocrJobsDisk = Storage::disk('ocrmypdfjobs');
        $this->s3disk = Storage::disk('s3');

        $jobDirectoryName = 'ocr-job-'.$this->ocrJob->uuid;

        // Create directories for each disk...
        $this->ocrJobsDisk->makeDirectory($jobDirectoryName);
        $this->s3disk->makeDirectory("{$this->ocrJob->uuid}");

        $originalPdfInputFilePath = $jobDirectoryName.'/original.pdf';

        $originalFileStream = $this->ocrJobsDisk
            ->writeStream(
                $originalPdfInputFilePath,
                Storage::readStream($this->ocrJob->original_path)
            );

        if (! $originalFileStream) {
            // Set the unknown exception on the ocr job...
            $this->ocrJob->setAsFailed(new UnknownException($this->ocrJob));

            // Delete the local disk's temporary directory...
            $this->ocrJobsDisk->deleteDirectory($jobDirectoryName);

            // Send the queue job to the failed_jobs table with the real exception...
            $this->fail(new S3StreamToLocalFailedException($this->ocrJob));

            return;
        }

        // Generate a temporary PDF with no content...
        $processedTempPdfOutputPath = $this->ocrJobsDisk->put(
            $jobDirectoryName.'/ocr.pdf',
            ''
        );

        // Generate a temporary OCR Text file with no content...
        $processedTempOcrTextOutputPath = $this->ocrJobsDisk->put(
            $jobDirectoryName.'/ocr.txt',
            ''
        );

        $command = [
            '/usr/local/bin/ocrmypdf',
            '--output-type pdf', // Creates a normal PDF instead of a PDF/A
            '--language eng', // Instructs Tesseract to OCR in English
            '--skip-text', // If a page already has text, skip it
            '--sidecar '.$jobDirectoryName.'/ocr.txt', // Allows for saving the OCR text to a separate file
            $originalPdfInputFilePath,
            $jobDirectoryName.'/ocr.pdf',
        ];

        $process = Process::timeout(1800)
            ->start(implode(' ', $command))
            ->wait();

        // Did the process fail?
        if ($process->exitCode() > 0) {
            // Delete the local disk's temporary directory...
            $this->ocrJobsDisk->deleteDirectory($jobDirectoryName);

            // Send the queue job to the failed_jobs table with the real exception...
            $class = OcrMyPdfException::EXCEPTIONS_MAP[$process->exitCode()] ?? OcrMyPdfException::EXCEPTIONS_MAP[15];
            $this->fail(new $class);

            return;
        }

        $ocrCompletedPdfS3StoragePath = $this->ocrJob->uuid.'/ocr.pdf';
        $processedUploadResult = $this->s3disk
            ->put($ocrCompletedPdfS3StoragePath, $processedTempPdfOutputPath);

        if (! $processedUploadResult) {
            // Delete the local disk's temporary directory...
            $this->ocrJobsDisk->deleteDirectory($jobDirectoryName);

            // Send the queue job to the failed_jobs table with the real exception...
            $this->fail(new S3UploadFailedException($this->ocrJob));

            return;
        }

        $this->ocrJob->setAsCompleted(
            $ocrCompletedPdfS3StoragePath,
            $this->ocrJobsDisk->get($processedTempOcrTextOutputPath) ?? null
        );

        event(new OcrJobCompleted($this->ocrJob));

        $this->ocrJobsDisk->deleteDirectory($jobDirectoryName);
    }

    /**
     * Laravel Queue Docs: "[Creates a] new instance of the
     *  job is instantiated before invoking the failed method; therefore, any
     *  class property modifications that may have occurred within the handle
     *  method will be lost."
     *
     * @see https://laravel.com/docs/10.x/queues#cleaning-up-after-failed-jobs
     */
    public function failed(OcrMyPdfException|Throwable $exception): void
    {
        // Determine which exception we should notify the project about...
        $notifiableException = (! in_array(get_class($exception), $this->shouldNotifyExceptions))
            ? new UnknownException($this->ocrJob)
            : $exception;

        event(new OcrJobFailed($this->ocrJob, $notifiableException));
    }
}
