<?php

namespace App\Providers;

use App\Events\WebhookEvents\OcrJobCompleted;
use App\Events\WebhookEvents\OcrJobCreated;
use App\Events\WebhookEvents\OcrJobFailed;
use App\Events\WebhookEvents\OcrJobStarted;
use App\Listeners\SendWebhookEvent;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event to listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        OcrJobCreated::class => [
            SendWebhookEvent::class,
        ],

        OcrJobStarted::class => [
            SendWebhookEvent::class,
        ],

        OcrJobCompleted::class => [
            SendWebhookEvent::class,
        ],

        OcrJobFailed::class => [
            SendWebhookEvent::class,
        ],
    ];

    /**
     * Register any events for your application.
     */
    public function boot(): void
    {
        //
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     */
    public function shouldDiscoverEvents(): bool
    {
        return false;
    }
}
