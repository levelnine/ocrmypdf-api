<?php

namespace Tests\Unit\Listeners;

use App\Events\WebhookEvents\OcrJobCompleted;
use App\Events\WebhookEvents\OcrJobCreated;
use App\Events\WebhookEvents\OcrJobFailed;
use App\Events\WebhookEvents\OcrJobStarted;
use App\Listeners\SendWebhookEvent;
use App\Models\OcrJob;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Spatie\WebhookServer\CallWebhookJob;
use Tests\TestCase;

class SendWebhookEventTest extends TestCase
{
    use RefreshDatabase;

    protected User $user;

    protected function setUp(): void
    {
        parent::setUp();

        Event::fake();
        Queue::fake();

        /** @var \App\Models\User $user */
        $this->user = User::factory()->create([
            'webhook_url' => 'https://example.com/webhooks/ocr',
        ]);

        $this->user->createToken('api-token');

        $this->assertEquals('https://example.com/webhooks/ocr', $this->user->webhook_url);
    }

    public function testSendWebhookEventListeningToEvents()
    {
        Event::assertListening(
            OcrJobCreated::class,
            SendWebhookEvent::class
        );

        Event::assertListening(
            OcrJobStarted::class,
            SendWebhookEvent::class
        );

        Event::assertListening(
            OcrJobCompleted::class,
            SendWebhookEvent::class
        );

        Event::assertListening(
            OcrJobFailed::class,
            SendWebhookEvent::class
        );
    }

    public function testQueuesWebhookOnOcrJobCreated()
    {
        /** @var \App\Models\OcrJob $ocrJob */
        $ocrJob = OcrJob::factory()->create([
            'user_id' => $this->user->id,
            'original_path' => 'lorem/ipsum.pdf',
        ]);

        $event = new OcrJobCreated($ocrJob);

        (new SendWebhookEvent())->handle($event);

        Queue::assertPushedOn(
            'webhooks',
            function (CallWebhookJob $job) use ($ocrJob) {
                return $job->webhookUrl === 'https://example.com/webhooks/ocr'
                    && $job->httpVerb === 'post'
                    && $job->payload['object'] === 'event'
                    && $job->payload['type'] === 'ocr.job.created'
                    && $job->payload['data']['id'] === $ocrJob->uuid
                    && blank($job->payload['data']['started_at'])
                    && $job->payload['data']['is_running'] === false
                    && $job->meta['ocr_job_uuid'] === $ocrJob->uuid;
            });
    }

    public function testQueuesWebhookOnOcrJobStarted()
    {
        /** @var \App\Models\OcrJob $ocrJob */
        $ocrJob = OcrJob::factory()->create([
            'user_id' => $this->user->id,
            'original_path' => 'lorem/ipsum.pdf',
            'started_at' => now(),
            'is_running' => true,
        ]);

        $event = new OcrJobStarted($ocrJob);

        (new SendWebhookEvent())->handle($event);

        Queue::assertPushedOn(
            'webhooks',
            function (CallWebhookJob $job) use ($ocrJob) {
                return $job->webhookUrl === 'https://example.com/webhooks/ocr'
                    && $job->httpVerb === 'post'
                    && $job->payload['object'] === 'event'
                    && $job->payload['type'] === 'ocr.job.started'
                    && $job->payload['data']['id'] === $ocrJob->uuid
                    && filled($job->payload['data']['started_at'])
                    && $job->payload['data']['is_running'] === true
                    && $job->meta['ocr_job_uuid'] === $ocrJob->uuid;
            });
    }

    public function testQueuesWebhookOnOcrJobCompleted()
    {
        Storage::fake('completed');

        /** @var \App\Models\OcrJob $ocrJob */
        $ocrJob = OcrJob::factory()->create([
            'user_id' => $this->user->id,
            'original_path' => 'lorem/ipsum.pdf',
        ]);

        $ocrJob->setQueueJobInformation(Str::random(), Str::uuid());
        $ocrJob->setAsStarted();
        $ocrJob->setAsCompleted('lorem/ipsum.pdf', 'lorem ipsum');

        $event = new OcrJobCompleted($ocrJob);

        (new SendWebhookEvent())->handle($event);

        Queue::assertPushedOn(
            'webhooks',
            function (CallWebhookJob $job) use ($ocrJob) {
                return $job->webhookUrl === 'https://example.com/webhooks/ocr'
                    && $job->httpVerb === 'post'
                    && $job->payload['object'] === 'event'
                    && $job->payload['type'] === 'ocr.job.completed'
                    && $job->payload['data']['id'] === $ocrJob->uuid
                    && filled($job->payload['data']['started_at'])
                    && filled($job->payload['data']['completed_at'])
                    && filled($job->payload['data']['completed_path'])
                    && $job->payload['data']['text'] === 'lorem ipsum'
                    && $job->payload['data']['completed'] === true
                    && $job->payload['data']['completed'] === true
                    && $job->payload['data']['is_running'] === false
                    && $job->meta['ocr_job_uuid'] === $ocrJob->uuid;
            });
    }
}
