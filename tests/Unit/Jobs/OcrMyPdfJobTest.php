<?php

namespace Tests\Unit\Jobs;

use App\Events\WebhookEvents\OcrJobCompleted;
use App\Events\WebhookEvents\OcrJobFailed;
use App\Events\WebhookEvents\OcrJobStarted;
use App\Exceptions\OCRmyPDF\OcrMyPdfException;
use App\Jobs\OcrMyPdfJob;
use App\Models\OcrJob;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Process;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Tests\TestCase;

class OcrMyPdfJobTest extends TestCase
{
    use RefreshDatabase;

    public function testOcrMyPdfQueueJobSuccessWithoutWebhook(): void
    {
        Process::preventStrayProcesses();

        Event::fake();

        // Use S3 Storage initially to mimic the FilesController
        Storage::fake('s3');

        $ocrJobUuid = Str::uuid();

        $originalFile = UploadedFile::fake()->create('original.pdf', 1024, 'application/pdf');

        Storage::makeDirectory($ocrJobUuid);

        $filePath = $originalFile->storeAs($ocrJobUuid, 'original.pdf', 's3');

        Storage::assertExists($ocrJobUuid.'/original.pdf');

        /** @var \App\Models\OcrJob $ocrJob */
        $ocrJob = OcrJob::factory()->create([
            'uuid' => $ocrJobUuid,
            'original_path' => (string) $filePath,
        ]);

        // Swap to local storage
        Storage::fake('ocrmypdfjobs');

        $jobDirectoryName = 'ocr-job-'.$ocrJob->uuid;

        $command = [
            '/usr/local/bin/ocrmypdf',
            '--output-type pdf',
            '--language eng',
            '--skip-text',
            "--sidecar $jobDirectoryName/ocr.txt",
            "$jobDirectoryName/original.pdf",
            "$jobDirectoryName/ocr.pdf",
        ];

        Process::fake([
            implode(' ', $command) => Process::result(),
        ]);

        dispatch(new OcrMyPdfJob($ocrJob));

        Process::assertRan(function ($process, $result) use ($command) {
            return $process->command === implode(' ', $command)
                && $result->successful();
        });

        Event::assertDispatched(OcrJobStarted::class);
        Event::assertDispatched(OcrJobCompleted::class);
    }

    public function testOcrMyPdfQueueJobFails(): void
    {
        Process::preventStrayProcesses();

        Event::fake();

        // Use S3 Storage initially to mimic the FilesController
        Storage::fake('s3');

        $ocrJobUuid = Str::uuid();

        $originalFile = UploadedFile::fake()->create('original.pdf', 1024, 'application/pdf');

        Storage::makeDirectory($ocrJobUuid);

        $filePath = $originalFile->storeAs($ocrJobUuid, 'original.pdf', 's3');

        Storage::assertExists($ocrJobUuid.'/original.pdf');

        /** @var \App\Models\OcrJob $ocrJob */
        $ocrJob = OcrJob::factory()->create([
            'uuid' => $ocrJobUuid,
            'original_path' => (string) $filePath,
        ]);

        // Swap to local storage
        Storage::fake('ocrmypdfjobs');

        $jobDirectoryName = 'ocr-job-'.$ocrJob->uuid;

        $command = [
            '/usr/local/bin/ocrmypdf',
            '--output-type pdf',
            '--language eng',
            '--skip-text',
            "--sidecar $jobDirectoryName/ocr.txt",
            "$jobDirectoryName/original.pdf",
            "$jobDirectoryName/ocr.pdf",
        ];

        Process::fake([
            implode(' ', $command) => Process::result(
                output: '',
                errorOutput: 'errors!',
                exitCode: OcrMyPdfException::EXIT_CODE_ENCRYPTED_PDF,
            ),
        ]);

        dispatch(new OcrMyPdfJob($ocrJob));

        Process::assertRan(function ($process, $result) use ($command) {
            return $process->command === implode(' ', $command)
                && $result->exitCode() === OcrMyPdfException::EXIT_CODE_ENCRYPTED_PDF;
        });

        Event::assertDispatched(OcrJobStarted::class);
        Event::assertDispatched(OcrJobFailed::class);

        Event::assertNotDispatched(OcrJobCompleted::class);
    }
}
