<?php

namespace Tests\Feature\Console\Commands;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CreateProjectCommandTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     */
    public function testCreateNewProjectWithToken(): void
    {
        $this->artisan('app:create-project')
            ->expectsQuestion('What is the project name?', 'Project A')
            ->expectsQuestion('What environment will this token be used for?', 'staging')
            ->expectsQuestion("What is the project's webhook URL?", 'https://example.com')
            ->expectsOutputToContain('Project A | staging');
    }
}
