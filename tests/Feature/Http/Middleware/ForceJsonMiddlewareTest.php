<?php

namespace Tests\Feature\Http\Middleware;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ForceJsonMiddlewareTest extends TestCase
{
    use RefreshDatabase;

    public function testNonJsonCallSuccess()
    {
        $this->get('/api/me')
            ->assertUnauthorized()
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testJsonRequestSuccess()
    {
        $this->getJson('/api/me')
            ->assertUnauthorized()
            ->assertJsonStructure([
                'message',
            ]);
    }
}
