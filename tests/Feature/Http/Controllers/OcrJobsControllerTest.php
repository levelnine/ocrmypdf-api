<?php

namespace Tests\Feature\Http\Controllers;

use App\Jobs\OcrMyPdfJob;
use App\Models\OcrJob;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class OcrJobsControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testSubmitFileRequiresAPdf()
    {
        Storage::fake('uploads');

        /** @var \App\Models\User $user */
        $user = User::factory()->create();

        $file = UploadedFile::fake()->image('lorem.jpg');

        Sanctum::actingAs($user, ['*']);

        $response = $this->postJson(route('ocr.jobs.new'), [
            'file' => $file,
        ]);

        $response->assertJsonValidationErrors(['file']);
    }

    public function testSubmitFilePdfIsRequired()
    {
        /** @var \App\Models\User $user */
        $user = User::factory()->create();

        Sanctum::actingAs($user, ['*']);

        $response = $this->postJson(route('ocr.jobs.new'));

        $response->assertJsonValidationErrors(['file']);
    }

    public function testSubmitFilePdfCannotBeGreaterThanTwoGb()
    {
        Storage::fake('uploads');

        /** @var \App\Models\User $user */
        $user = User::factory()->create();

        $file = UploadedFile::fake()->create('lorem.pdf', 2100555, 'application/pdf');

        Sanctum::actingAs($user, ['*']);

        $response = $this->postJson(route('ocr.jobs.new'), [
            'file' => $file,
        ]);

        $response->assertJsonValidationErrors(['file']);
    }

    public function testSubmitFilePdfSuccess()
    {
        Queue::fake();
        Storage::fake('uploads');

        /** @var \App\Models\User $user */
        $user = User::factory()->create();

        $file = UploadedFile::fake()->create('lorem.pdf', 1024, 'application/pdf');

        Sanctum::actingAs($user, ['*']);

        $response = $this->postJson(route('ocr.jobs.new'), [
            'file' => $file,
        ]);

        $response->assertCreated();

        Queue::assertPushedOn('ocr', function (OcrMyPdfJob $job) use ($response) {
            return $job->ocrJob->uuid === $response->json('data.id');
        });
    }

    public function testGetFileStatusNotStarted()
    {
        Storage::fake('uploads');

        /** @var \App\Models\User $user */
        $user = User::factory()->create();

        $fileUpload = UploadedFile::fake()->create('lorem.pdf', 1024, 'application/pdf');

        /** @var \App\Models\OcrJob $ocrJob */
        $ocrJob = OcrJob::factory()->create([
            'original_path' => $fileUpload,
            'user_id' => $user->id,
        ]);

        Sanctum::actingAs($user, ['*']);

        $response = $this->getJson(route('ocr.jobs.status', $ocrJob));

        $response->assertOk();

        $response->assertJsonFragment([
            'id' => $ocrJob->uuid,
            'completed' => false,
            'text' => null,
            'download_url' => null,
            'is_running' => false,
        ]);
    }

    public function testGetFileStatusOcrJobStarted()
    {
        Storage::fake('uploads');

        /** @var \App\Models\User $user */
        $user = User::factory()->create();

        $uuid = Str::uuid();

        $fileUpload = UploadedFile::fake()->create('lorem.pdf', 1024, 'application/pdf')
            ->storeAs('uploads', "$uuid.pdf");

        $this->assertNotFalse(
            $fileUpload,
            'There seems to be an issue with the fake PDF upload'
        );

        /** @var \App\Models\OcrJob $ocrJob */
        $ocrJob = OcrJob::factory()->create([
            'uuid' => $uuid,
            'original_path' => $fileUpload,
            'user_id' => $user->id,
        ]);

        $ocrJob->setAsStarted();

        Sanctum::actingAs($user, ['*']);

        $response = $this->getJson(route('ocr.jobs.status', $ocrJob));

        $response->assertOk();

        $this->assertNotNull(
            $response->json('data.started_at'),
            'The OcrJob started_at is null.'
        );

        $response->assertJsonFragment([
            'id' => $ocrJob->uuid,
            'completed' => false,
            'is_running' => true,
        ]);
    }

    public function testGetFileStatusOcrJobCompleted()
    {
        Storage::fake('uploads');

        /** @var \App\Models\User $user */
        $user = User::factory()->create();

        $uuid = Str::uuid();

        $fileUpload = UploadedFile::fake()->create('lorem.pdf', 1024, 'application/pdf')
            ->storeAs('uploads', "$uuid.pdf");

        $this->assertNotFalse(
            $fileUpload,
            'There seems to be an issue with the fake PDF upload'
        );

        /** @var \App\Models\OcrJob $ocrJob */
        $ocrJob = OcrJob::factory()->create([
            'uuid' => $uuid,
            'original_path' => $fileUpload,
            'user_id' => $user->id,
        ]);

        $ocrJob->setAsStarted();
        $ocrJob->setAsCompleted('lorem/ipsum.pdf', 'lorem ipsum');

        $this->assertNotNull($ocrJob->started_at, 'The Ocr Job started_at is null.');
        $this->assertNotNull($ocrJob->completed_at, 'The Ocr Job completed_at is null.');
        $this->assertFalse($ocrJob->is_running, 'The Ocr Job is_running is true.');
        $this->assertNull($ocrJob->failed_at, 'The Ocr Job failed_at is not null.');
        $this->assertNull($ocrJob->exception, 'The Ocr Job exception is not null.');

        Sanctum::actingAs($user, ['*']);

        $response = $this->getJson(route('ocr.jobs.status', $ocrJob));

        $response->assertOk();

        $this->assertNotNull(
            $response->json('data.started_at'),
            'The OcrJob started_at is null.'
        );

        $this->assertNotNull(
            $response->json('data.completed_at'),
            'The OcrJob completed_at is null.'
        );

        $response->assertJsonFragment([
            'id' => $ocrJob->uuid,
            'completed' => true,
            'is_running' => false,
        ]);
    }

    public function testDownloadFileSuccess()
    {
        Storage::fake('uploads');

        /** @var \App\Models\User $user */
        $user = User::factory()->create();

        $uuid = Str::uuid();

        $fakeFile = UploadedFile::fake()->create('lorem.pdf', 1024, 'application/pdf');

        $fileUpload = $fakeFile->storeAs('uploads', "$uuid.pdf");

        $this->assertNotFalse(
            $fileUpload,
            'There seems to be an issue with the fake PDF upload'
        );

        /** @var \App\Models\OcrJob $ocrJob */
        $ocrJob = OcrJob::factory()->create([
            'uuid' => $uuid,
            'original_path' => $fileUpload,
            'completed_path' => $fileUpload,
            'user_id' => $user->id,
            'started_at' => now(),
            'completed_at' => now(),
        ]);

        Sanctum::actingAs($user, ['*']);

        $response = $this->getJson($ocrJob->downloadUrl());

        $response->assertStreamedContent($fakeFile->getContent());
    }
}
