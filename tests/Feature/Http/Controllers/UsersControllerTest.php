<?php

namespace Tests\Feature\Http\Controllers;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class UsersControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testUnauthenticated()
    {
        $this->getJson('/api/me')
            ->assertUnauthorized();
    }

    public function testAuthenticatedMeEndpoint()
    {
        /** @var \App\Models\User $user */
        $user = User::factory()->create();

        Sanctum::actingAs($user, ['*']);

        $this->getJson('/api/me')
            ->assertOk()
            ->assertJsonFragment([
                'id' => $user->uuid,
                'name' => $user->name,
                'environment' => $user->environment,
            ]);
    }
}
