<?php

return [
    'exceptions_via_webhooks' => [
        \App\Exceptions\OCRmyPDF\OcrMyPdfException::EXIT_CODE_ALREADY_DONE_OCR,
        \App\Exceptions\OCRmyPDF\OcrMyPdfException::EXIT_CODE_ENCRYPTED_PDF,
        \App\Exceptions\OCRmyPDF\OcrMyPdfException::EXIT_CODE_PDFA_CONVERSION_FAILED,
    ],
];
