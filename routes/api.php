<?php

use App\Http\Controllers\OcrJobsController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;

Route::get('/me', [UsersController::class, 'view'])
    ->middleware('auth:sanctum')
    ->name('users.me');
Route::post('/jobs/new', [OcrJobsController::class, 'store'])
    ->middleware('auth:sanctum')
    ->name('ocr.jobs.new');
Route::get('/jobs/status/{ocrJob}', [OcrJobsController::class, 'view'])
    ->middleware('auth:sanctum')
    ->name('ocr.jobs.status');
Route::get('/jobs/download/{ocrJob}', [OcrJobsController::class, 'download'])
    ->middleware(['auth:sanctum', 'signed'])
    ->name('ocr.jobs.file.download');
